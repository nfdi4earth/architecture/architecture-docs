# Introduction and Goals

<!---
Describes the relevant requirements and the driving forces that software architects and development team must consider. These include
- underlying business goals,
- essential features,
- essential functional requirements,
- quality goals for the architecture and
- relevant stakeholders and their expectations
-->

The mission of NFDI4Earth is to address the digital needs of the Earth System Sciences (ESS) for more FAIRness and Openness in ESS research. (Read and sign our commitment here: https://nfdi4earth.de/commitment)
We develop several services, and concepts within NFDI4Earth and reuse/integrate existing services when suitable. By doing so, we enable researchers, data experts, and software specialists to discover, access, analyse, and share relevant Earth data and related publications or tools.

NFDI4Earth supports the following (main) use cases with common services: <br>
1. Discover and explore earth data sources <br>
2. Support data publication and data curation <br>
3. Solve a research data management problem <br>
4. Create and publish information products, e.g., as services <br>

The architecture of the NFDI4Earth describes the different services built to make resources from the Earth System Sciences (ESS) findable, accessible, interoperable and reusable, as well as the requirements for interfaces enabling their interaction.<br>

In NFDI4Earth, we follow the service definition used in the [joint statement of NFDI consortia on basic services](https://doi.org/10.5281/zenodo.6091657):<br>
  >*A service in NFDI is understood as a technical-organisational solution, which typically includes storage and computing services, software, processes,  and workflows, as well as the necessary personnel support for different service desks.*

The service portfolio is described in Section Solution Strategy.

## Quality Goals

<!---
**Contents**

The top three (max five) quality goals for the architecture whose
fulfillment is of highest importance to the major stakeholders. We
really mean quality goals for the architecture. Don't confuse them with
project goals. They are not necessarily identical.

Consider this overview of potential topics (based upon the ISO 25010
standard):

![Categories of Quality
Requirements](images/01_2_iso-25010-topics-EN.drawio.png)

**Motivation**

You should know the quality goals of your most important stakeholders,
since they will influence fundamental architectural decisions. Make sure
to be very concrete about these qualities, avoid buzzwords. If you as an
architect do not know how the quality of your work will be judged...

**Form**

A table with quality goals and concrete scenarios, ordered by priorities
-->

<!---
See https://iso25000.com/index.php/en/iso-25000-standards/iso-25010 for a full list and https://www.monterail.com/blog/software-qa-standards-iso-25010 for some more background/discussion.
-->

In this section, we describe quality goals synonymously used as a term to describe architecture goals with a long-term perspective. As the NFDI4Earth Software Architecture is evolving, we envision to regularly evaluate the prioritization of the quality goals. Following the ISO25010 on software product quality, we consider the following quality goals for the NFDI4Earth Software Architecture:

**Priority** | **Goal** | **Description**
------------ | -------- | -------------
- **Functional suitability**: Degree to which the architecture provides functions that meet stated and implied needs when used under specified conditions
- **Maintainability**: Degree of effectiveness and efficiency with which the architecture can be modified to improve it, correct it or adapt it to changes in environment, and in requirements
- **Usability**: Degree to which a component can be used by specified users to achieve specified goals with effectiveness, efficiency and satisfaction in a specified context of use.

## Stakeholders

<!--- 
**Contents**

Explicit overview of stakeholders of the system, i.e. all person, roles or organizations that

- should know the architecture
- have to be convinced of the architecture
- have to work with the architecture or with code
- need the documentation of the architecture for their work
- have to come up with decisions about the system or its development

**Motivation**

You should know all parties involved in development of the system or affected by the system. Otherwise, you may get nasty surprises later in the development process. These stakeholders determine the extent and the level of detail of your work and its results.

**Form**

Table with role names, person names, and their expectations with respect to the architecture and its documentation.

+-------------+---------------------------+---------------------------+
| Role/Name   | Contact                   | Expectations              |
+=============+===========================+===========================+
| *\<Role-1>* | *\<Contact-1>*            | *\<Expectation-1>*        |
+-------------+---------------------------+---------------------------+
| *\<Role-2>* | *\<Contact-2>*            | *\<Expectation-2>*        |
+-------------+---------------------------+---------------------------+
-->
We consider the following roles for using the NFDI4Earth Software Architecture Documentation: 

**Role/Name** | **Expectations**
------------- |  ----------------
Internal developers of NFDI4Earth services | Find descriptions and specifications of other NFDI4Earth-developed services and integrate the services within their own projects |
External developers | Find specifications on how to use existing NFDI4Earth-developed services and find descriptions on how to add services to the NFDI4Earth infrastructure |

