# NFDI4Earth System Architecture Documentation

Here we provide a documentation of the NFDI4Earth software architecture. The documentation summarizes the major user cases, gives an overview on the requirement analysis approaches for the main NFDI4Earth software components, points out envisioned quality goals and potential architecture’s stakeholders. Getting more specific, the documentation provides externally and internally driven constraints that need to be considered when developing or contributing to NFDI4Earth software components. We herewith facilitate, the users to better understand the solution strategy for the NFDI4Earth architecture. 
We use the arc42 template for the documentation of software and system architecture. The detailed description of the NFDI4Earth architecture is done by using a blackbox-whitebox approach that describes the major aim, the main functionalities and interfaces of the components as seen by the users first (blackbox) and then provides details on the (inner) solutions, e.g., sub-components, implementations (whitebox).
The envisioned target group of the documentation includes software developers and architects as well service providers.

The architecture documentation will be updated regularly.<br> 
The current version of the NFDI4Earth architecture documentation is published on Zenodo: 
Henzen, C., Brauer, A., Degbelo, A., Frickenhaus, S., Grieb, J., Hachinger, S., Klammer, R., Müller, C., Munke, J., Niers, T., Nüst, D., Weiland, C., & Wellmann, A. (2024). NFDI4Earth Software Architecture Documentation. Zenodo. https://doi.org/10.5281/zenodo.14534839 

Contact: [Christin Henzen](mailto:christin.henzen@tu-dresden.de) or the [NFDI4Earth Architecture Team](mailto:nfdi4arth-architecture@tu-dresden.de) with your questions and comments.

<!--- https://git.rwth-aachen.de/nfdi4earth/softwaretoolsarchitecture/architecture-docs/-/issues/2
[PDF](nfdi4earth-architecture.pdf)
-->

{% include '01_introduction_and_goals.md' %}
{% include '02_architecture_constraints.md' %}
{% include '03_system_scope_and_context.md' %}
{% include '04_solution_strategy.md' %}
{% include '05_building_block_view.md' %}
{% include '06_runtime_view.md' %}
{% include '07_deployment_view.md' %}
{% include '08_concepts.md' %}
{% include '09_architecture_decisions.md' %}
{% include '10_quality_requirements.md' %}
{% include '11_technical_risks.md' %}

{% include 'acknowledgements.md' %}
