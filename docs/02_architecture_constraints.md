# Architecture Constraints

<!---
**Contents**

Any requirement that constraints software architects in their freedom of design and implementation decisions or decision about the development process.
These constraints sometimes go beyond individual systems and are valid for whole organizations and companies.

**Motivation**

Architects should know exactly where they are free in their design decisions and where they must adhere to constraints.
Constraints must always be dealt with; they may be negotiable, though.

**Form**

Simple tables of constraints with explanations.
If needed you can subdivide them into technical constraints, organizational and political constraints and conventions (e.g. programming or versioning guidelines, documentation or naming conventions).

See [Architecture Constraints](https://docs.arc42.org/section-2/) in the arc42 documentation.

-->

TThe following requirements constrain the design and implementation decisions and processes for the NFDI4Earth Software Architecture:

**Constraint** | **Type** | **Explanation**
-------------- | -------- | ---------------
NFDI4Earth Proposal | organisational, strategic and technical  | The proposal provides the context and aims of the NFDI4Earth Software Architecture (see https://doi.org/10.5281/zenodo.5718943). 
NFDI integration and interoperability | technical, strategic | The NFDI4Earth Software Architecture must fit with relevant activities of the overall NFDI, e.g., with basic service initiatives.
International integration and interoperability | technical, strategic | The NFDI4Earth software architecture will be embedded in international infrastructures.
Developer expertise, research interests, and availability | organisational, technical | The expertise, research interests, and availability of a distributed software developer team affect the software project management as well as the technology decisions.
Architecture team | organisational | Software decisions for NFDI4Earth-developed services are made by the NFDI4Earth architecture team on suggestion and on agreement with the measure leads of the relevant service.
Developer team | organisational, conventions | NFDI4Earth cross-product implementations, guidance and conventions will be provided by the software developer team.
FAIR principles | technical | NFDI4Earth services should support the implementation of the FAIR principles.
Programming languages | technical | Services will be developed in an established programming language, e.g., JavaScript, Java, Python, C#, HTML, and follow the basic structures of a sustainable software project, e.g., testing, dependency management, documentation and internationalisation
Free and Open Source Software | technical and strategical | NFDI4Earth-developed services must be provided as Free and Open Source (FOSS). Used open-source solutions must be well established, documented and maintained, e.g., guaranteed by their long-term applicability whenever possible.
Open standards / specifications | technical | NFDI4Earth services will reuse existing (preferably) or define open standards or specifications for all interfaces, when interfaces are relevant for the service 
Loosely coupled services | technical | NFDI4Earth services must be loosely coupled to allow replacing software solutions, e.g., as agile adaptions to user needs and changing requirements or newly developed NFDI-wide solutions 
Software repository | organisational | The source code of the NFDI4Earth-developed services must be managed in a software repository that allows contributions from (relevant) NFDI4Earth participants
Hosting at TU Dresden | technical, organisational | The NFDI4Earth-developed services are hosted at TU Dresden Enterprise cloud and maintained by following the respective guidelines / regulations (https://git.rwth-aachen.de/nfdi4earth/softwaretoolsarchitecture/devguide).
Containerization | technical | NFDI4Earth services run in virtual containers, whenever possible.  

**Types**: technical constraints, organisational constraints, strategic constraints, and conventions (e.g., programming or versioning guidelines, documentation or naming conventions).
