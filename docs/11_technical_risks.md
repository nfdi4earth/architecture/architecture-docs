# Risks and Technical Debts

<!---
**Contents**

A list of identified technical risks or technical debts, ordered by priority

**Motivation**

"Risk management is project management for grown-ups" (Tim Lister, Atlantic Systems Guild.)

This should be your motto for systematic detection and evaluation of risks and technical debts in the architecture, which will be needed by management stakeholders (e.g. project managers, product owners) as part of the overall risk analysis and measurement planning.

**Form**

List of risks and/or technical debts, probably including suggested measures to minimize, mitigate or avoid risks or reduce technical debts.

See [Risks and Technical Debt](https://docs.arc42.org/section-11/) in the arc42 documentation.
-->

_Risks and technical debts to be added here as they come up._
The systematic detection and evaluation of risks and technical debts in the architecture will serve management stakeholders in the overal risk analysis and planning.

**Name** | **Priority** (high, medium, low) | **Measures** to minimize, mitigate, avoid, or reduce
-------- | -------------------------------- | ----------------------------------------------------
... | ... | ...

**Priorities**: _high_ risks or debts are critical and can break the whole project, _medium_ risks or debts can cause serious extra efforts but can be fixed even if one must go outside of the given resources, and _low_ risks or debts are uncertainties that can be handled within the normal project resources.
