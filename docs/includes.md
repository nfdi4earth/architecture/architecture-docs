{% macro smalltext(text) -%}
<small style="opacity: .5">{{ text }}</small>
{%- endmacro %}
{% macro figure_caption(text) -%}
<small style="opacity: .5"><i>{{ text }}</i></small>
{%- endmacro %}

{% macro highlighted_link(note, url) -%}
!!! note "{{ note }}"
    [Click here]({{ url }})
{%- endmacro %}

{% macro highlighted_linklist(note, linklist=[]) -%}
!!! note "{{ note }}"
    {% for link in linklist %}
    - [{{ link.name }}]({{ link.url }})
    {% endfor %}
{%- endmacro %}


{% macro load_docker_info(filename) -%}
{% set content = load_jsonfile(filename, 'docker') %}
{% set export_note = 'This data has been exported on ' + content.date %}
{% for c in content.containers %}
- {{ c.Names[0][1:] }}
    - Image: {{ c.Image }}{% if c.Ports %}
    - Ports:{% for p in c.Ports %}
        - {{ p }}{% endfor %}{% endif %}{% endfor %}

{{ smalltext(export_note) }}
{%- endmacro %}

{% macro load_sys_info(filename) -%}
{% set content = load_jsonfile(filename, 'system') %}
{% set sysinfo = content.sysinfo %}
{% set export_note = 'This data has been exported on ' + content.date %}

- IP: {{ sysinfo.ip }}
- System: {{ sysinfo.system }}
- CPUs: {{ sysinfo.cpus }}
- Cores: {{ sysinfo.cores }}
- Memory: {{ sysinfo.memory }} MB
- Disk:
    - Total: {{ sysinfo.disk.total }} GB
    - Available: {{ sysinfo.disk.available }} GB ({{ sysinfo.disk.percentage }}%)
- Hostname: {{ sysinfo.hostname }}
- Ansible inventory hostname: {{ sysinfo.inventory_hostname }}

{{ smalltext(export_note) }}
{%- endmacro %}

{% macro load_ssl_info(filename) -%}
{% set content = load_jsonfile(filename, 'ssl') %}
{% set export_note = 'This data has been exported on ' + content.date %}

{{ content.infos }}

<i>Output of `systemctl list-timers | grep certbot`:</i>

> {{ content.timer }}

{{ smalltext(export_note) }}
{%- endmacro %}

{% macro load_gitlab_maingroups() -%}
{% set content = get_gitlab_maingroups() %}
{% set export_note = 'This data has been exported on ' + content.date %}

<ul>
{% for group in content['groups'] %}
<li>
    <a href="{{ group.webUrl }}">{{ group.name }}</a>
</li>
{% endfor %}
</ul>

{{ smalltext(export_note) }}
{%- endmacro %}


{% macro load_gitlab_user_stats() -%}
{% set content = get_gitlab_user_stats() %}
{% set export_note = 'This data has been exported on ' + content.date %}
<style>
    #gitlab_user th:first-child,
    #gitlab_user td:first-child {
        position:sticky;
        left:0px;
    }
    #gitlab_user tr:nth-child(2n)>td:first-child {
        background-color: white;
    }    
</style>
<div style="overflow-x: scroll; width: 100%;">
    <table id="gitlab_user">
    <tbody>
        <tr>
            <th></th>
            {% for user in content['users'] %}
            <th>
                <a href="{{ user.url }}" target="_blank">{{ user.name }}</a>
            </th>
            {% endfor %}
        </tr>
        {% for group in content['groups'] %}
        <tr>
            <td>
                <a href="{{ group[0].url }}" target="_blank">{{ group[0].name }}</a>
            </td>
            {% for entry in group[1:] %}
            <td class="text-center">{{ entry }}</td>
            {% endfor %}
        </tr>
        {% endfor %}
    </tbody>
    </table>
</div>

{{ smalltext('Overview of members in GitLab groups') }}

{{ smalltext(export_note) }}
{%- endmacro %}
