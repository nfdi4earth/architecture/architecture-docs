# Building Block View

<!---
**Content**

The building block view shows the static decomposition of the system into building blocks (modules, components, subsystems, classes, interfaces, packages, libraries, frameworks, layers, partitions, tiers, functions, macros, operations, data structures, ...) as well as their dependencies (relationships, associations, ...)

This view is mandatory for every architecture documentation.
In analogy to a house this is the *floor plan*.

**Motivation**

Maintain an overview of your source code by making its structure understandable through abstraction.

This allows you to communicate with your stakeholder on an abstract level without disclosing implementation details.

**Form**

The building block view is a hierarchical collection of black boxes and white boxes (see figure below) and their descriptions.

![Hierarchy of building blocks](images/05_building_blocks-EN.png)

**Level 1** is the white box description of the overall system together with black box descriptions of all contained building blocks.

**Level 2** zooms into some building blocks of level 1.
Thus it contains the white box description of selected building blocks of level 1, together with black box descriptions of their internal building blocks.

**Level 3** zooms into selected building blocks of level 2, and so on.

See [Building Block View](https://docs.arc42.org/section-5/) in the
arc42 documentation.
-->

## Whitebox Overall System

<!---
Here you describe the decomposition of the overall system using the
following white box template. It contains

- an overview diagram
- a motivation for the decomposition
- black box descriptions of the contained building blocks. For these we offer you alternatives:
  - use *one* table for a short and pragmatic overview of all contained building blocks and their interfaces
  - use a list of black box descriptions of the building blocks according to the black box template (see below). Depending on your choice of tool this list could be sub-chapters (in text files), sub-pages (in a Wiki) or nested elements (in a modeling tool).
- (optional:) important interfaces, that are not explained in the black box templates of a building block, but are very important for understanding the white box. Since there are so many ways to specify interfaces why do not provide a specific template for them. In the worst case you have to specify and describe syntax, semantics, protocols, error handling, restrictions, versions, qualities, necessary compatibilities and many things more. In the best case you will get away with examples or simple signatures.

-->

The NFDI4Earth software architecture provides two services that serve as entry points to several linked NFDI4Earth and community-developed services: the OneStop4All as human-readable interactive user interface and the Knowledge Hub as machine-interpretable interface.

![NFDI4Earth architecture](images/NFDI4Earth_Architecture.JPG)

<!---
If you use tabular form you will only describe your black boxes with
name and responsibility according to the following schema:

**Name** | **Responsibility** | **Interfaces**
-------- | --------------- | --------------
*black box 1* | ... | ...
*black box 2* | ... | ...
-->

<!---
If you use a list of black box descriptions then you fill in a separate
black box template for every important building block . Its headline is
the name of the black box.

### *Name black box 1*

Here you describe *black box 1* according the the following black box
template:

- Purpose/Responsibility
- Interface(s), when they are not extracted as separate paragraphs.This interfaces may include qualities and performance characteristics.
- (Optional) Quality-/Performance characteristics of the black box, e.g.availability, run time behavior, ....
- (Optional) directory/file location
- (Optional) Fulfilled requirements (if you need traceability to requirements).
- (Optional) Open issues/problems/risks
-->

<!---

&nbsp; | &nbsp;
------ | ------
**Purpose/Responsibility** | ...
**Interface(s)** | ...
**(Optional) Quality/Performance Characteristics** | ...
**(Optional) Directory/File Location>** | ...
**(Optional) Fulfilled Requirements>** | ...
**(Optional) Open Issues/Problems/Risks** | ...

-->

### Blackbox Knowledge Hub

The blackbox description is based on the Knowledge Hub concept one-pager and concept deliverable ([https://doi.org/10.5281/zenodo.7950859](https://doi.org/10.5281/zenodo.7950859); [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596)). 

The NFDI4Earth Knowledge Hub serves as one major backend service of NFDI4Earth. It integrates metadata about all NFDI4Earth resources and is accessed via an API.

&nbsp; | &nbsp;
------ | ------
**Problem** | Research products from the Earth System Sciences (ESS) are increasingly difficult to find. There is a need for tools that automate their discovery. ‘Research products’ is used here as a catch-all term that includes 1) datasets, 2) services, 3) tools, 4) vocabularies, 5) reports, 6) scientific papers, and 7) peer reviews, etc. . 
**Innovations** | **Structured and interlinked metadata** produced in NFDI4Earth or relevant for the NFDI4Earth. These ESS resources can be any research product listed above, but also an article of the Living Handbook, or an educational material from the EduTrain. We use RDF (Resource Description Framework) as an encoding format. <br><br>Structured and interlinked metadata for ESS resources **hosted by NFDI4Earth partners** <br><br>**NFDI4Earth label** - compiled based on the available metadata - as an indicator of the extent to which services are FAIR, and in particular, the degree of interoperability of the services
**Users** | **Consumers**: people who have basic skills in programmatic data access (i.e., they are able to program a short snippet of code in a programming language to retrieve data). <br><br> **Producers**: these create/edit metadata for the Knowledge Hub. They may have programming skills (in which case they create/edit metadata via the API of the Knowledge Hub) or have no programming skills (in which case they do the creation/editing via a user interface).
**Interface(s)** | SPARQL API
**Unit of adoption** | Individuals (e.g., data scientists); <br> Organizations (indirectly, as a by-product of the adoption by individuals) 

The NFDI4Earth Knowledge Hub is available at: <https://knowledgehub.nfdi4earth.de>

### Blackbox OneStop4All

The blackbox description is based on OneStop4All concept one-pager [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596).

The NFDI4Earth OneStop4All is the primary visual and user-friendly NFDI4Earth access point.

&nbsp; | &nbsp;
------ | ------
**Challenges** | Research products from the ESS community are diverse and increasingly difficult to find. There is thus a need for platforms that efficiently organize the access to ESS resources, in particular quality-assured resources. These platforms should be: <br><br> **User-friendly and easy-to-use**, taking specific user characteristics and needs into account <br> **Flexible** enough to integrate future RDM services (e.g., address multidisciplinary use cases with other NFDIs, link to EOSC services).
**Innovations** | **Central search** on NFDI4Earth resources and distributed sources, including relevant governmental, research and other open data sources <br><br> **Innovative user interfaces** to explore the linked ESS resources that adapt to the needs of different user groups <br><br> **Intelligent functionality** to connect Living Handbook information for registered resources <br><br> **Seamless transition** from machine-based to human-based support <br><br> A **community tool** fostering the sharing of high-quality information and resources 
**Users** | We envision the following types of primary users: <br>Users, who are **looking for ESS research** and ESS RDM information, e.g., events, networks	<br>Users, who are **looking for support**, e.g., on NFDI4Earth tools or on how to use NFDI4Earth services <br>Users, who want to **offer information/research products**<br> Users, who want to **provide feedback** on the content
**Interface(s)** | User interface
**Unit of adoption** | Individuals

The NFDI4Earth OneStop4All is available at: <https://onestop4all.nfdi4earth.de/>

### Blackbox EduTrain

The blackbox description is based on the EduTrain concept one-pager [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596).

The NFDI4Earth EduTrain provides a comprehensive overview on existing education and training material and will provide FAIR, Open, ready-to-use modular course material that are developed by the EduTrain team based on the community's needs.

&nbsp; | &nbsp;
------ | ------
**Problem** | A lack of FAIR and open educational resources is one of the biggest obstacles to scientific activities. Although substantial effort has already been put into developing Open Educational Resources (OERs), many issues still exist, e.g., peer-reviewing the content, maintenance responsibility, quality control, management, and lack of funding for the development and maintenance. Another major problem is that most existing FAIR principles and Open Science materials are generic. At the same time, ESS-specific materials that outline adapting the FAIR principles and Open Science concepts are highly needed but mainly missing.
**Innovations** | Development and maintenance of **OERs and curriculum** based on regular educational needs assessment of the ESS community <br><br>**Continuous collection and evaluation** of existing OERs in research data management tailored for ESS, spatio-temporal data literacy, and spatio-temporal data science <br><br> Funding the **development of new open-licensed materials** to meet the educational needs of the ESS community by publishing calls for educational pilots <br><br> Development of **target group-specific curricula**
**Users** | Scientists, ranging from early-career researchers (Ph.D. students, Post-Docs) to experienced senior scientists and professors <br>Master students <br>Bachelor students <br>Educators and training professionals (e.g., professors, lecturers, teaching assistants)
**Interface(s)** | User interfaces
**Unit of adoption** | Individuals <br> Higher education institutions <br> Research centres

The EduTrain Service is available at: <https://edutrain.nfdi4earth.de>

## Whitebox NFDI4Earth-developed Services

<!---
Here you can specify the inner structure of (some) building blocks from level 1 as white boxes.

You have to decide which building blocks of your system are important enough to justify such a detailed description.
Please prefer relevance over completeness.
Specify important, surprising, risky, complex or volatile building blocks.
Leave out normal, simple, boring or standardized parts of your system.
-->

Here, we describe the implementation solutions for the NFDI4Earth-developed services and the NFDI4Earth-funded Community Software.

### White Box Knowledge Hub

The NFDI4Earth Knowledge Hub consists of three building blocks to harvest, process and provide metadata.
The pre-processing scripts mainly provide pipelines to harvest data sources or populate manually collected metadata, map metadata to the NFDI4Earth schemas and add/update the harmonised metadata in the data management system. 
These scripts are written in Python and provided as open-source in the NFDI4Earth GitLab.
Through the use of a data management system that stores all manually-created and harvested metadata, the NFDI4Earth software architecture supports the management and provision of FAIR digital objects. 
The triple store stores all metadata as semantically-enriched metadata in RDF (Resource Description Framework) and is accessible through a SPARQL API.
The implementation of the data management system and the triple store happens in NFDI4Earth through open-source software.

![Whitebox Knowledge Hub](images/NFDI4Earth_Knowledge_Hub.png) 

The NFDI4Earth Ontology is available at: <https://nfdi4earth.de/ontology>. The Ontology is iteratively developed in the open Knowledge Hub working group.
The Knowledge Hub source code is managed in the NFDI4Earth GitLab: <https://git.rwth-aachen.de/nfdi4earth/knowledgehub>. Developments, e.g., harvester implementations, are coordinated across the products in the NFDI4Earth developer meeting: <https://www.nfdi4earth.de/2coordinate/software-developer-team>  


### White Box OneStop4All

The OneStop4All provides the Web frame for all NFDI4Earth user interface (UI) components. Thus, the OneStop4All links or embeds the EduTrain learning management system and the Living Handbook user interfaces with respect to a user-friendly navigation and a common look&feel for all NFDI4Earth UI components and provides access to the User Support Network via Web form. By doing so, the OneStop4All does not provide the exclusive, but an additional access point for all other NFDI4Earth software components with user interfaces.
The central search on all NFDI4Earth resources provides the core functionality of the OneStop4All.
The OneStop4All is implemented as a custom solution.

The design and implementation strategy are described here: <https://doi.org/10.5281/zenodo.10351658>, <https://doi.org/10.5281/zenodo.13629130>
OneStop4All source code is managed in the NFDI4Earth GitLab: <https://git.rwth-aachen.de/nfdi4earth/onestop4all>. Developments are coordinated across the products in the NFDI4Earth developer meeting: <https://www.nfdi4earth.de/2coordinate/software-developer-team>  

![Whitebox OneStop4All](images/NFDI4Earth_OneStop4All.png) 


### Blackbox Living Handbook

The blackbox description is based on the Living Handbook concept one-pager [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596).

The NFDI4Earth Living Handbook provides an interactive Web-based documentation for all aspects related to the NFDI4Earth, its services and outcomes.

&nbsp; | &nbsp;
------ | ------
**Problem** | Many researchers, societies, funding agencies, companies, authorities, or the interested public are not familiar with each aspect of the NFDI4Earth, its services, or ESS research data in general. A core service with overview documents of such topics is, hence, required. The various user needs, and prior knowledges must be reflected in these documents, i.e., these must provide a flexible granularity, from being brief and informal to being comprehensive and detailed.
**Innovations** |	**Structuring and harmonizing** all aspects of NFDI4Earth as well as ESS related information from different, also previously unpublished, sources. <br><br> **Curate and present** information about the NFDI4Earth as a collection of edited, inter-linked, human-readable documents of various types (documentation, report, article, manual, tutorial, ed-op, etc.) that are externally linked with general ESS resources. <br><br>Compilation of documents tailored to the different **proficiency levels and backgrounds of readers** by a combination of automatic re-combination and re-arrangement of the document's elements. 
**Users** | **Consumers**: Users with interest in NFDI4Earth, NFDI or else ESS related information, data, services, concepts, software etc. We expect users with a high variety in their backgrounds and prior knowledge. <br>**Editors/authors**: Persons that provide and regularly quality check the LHB contents.
**Interface(s)** | User interface
**Unit of adoption** | The LHB is beneficial to, e.g.,: <br> **Researchers** as a manual how to use NFDI4Earth and related external products and to learn what the scope of the NFDI4Earth and its services are. <br>**Scientific and professional societies** as a place to refer their members as a resource for ESS data related topics. <br> **Funding agencies** to understand how researchers are using and providing ESS data <br> **Authorities** to get and provide information about ESS data **The interested public** as the first stop to find ESS related information

The NFDI4Earth Living Handbook is managed here: https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook

### Blackbox User Support Network

The blackbox description is based on the User Support Network concept one-pager [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596).

The NFDI4Earth User Support Network provides distributed, cross-institutional user support based on the services of the existing partner institutions’ services and the upcoming NFDI4Earth innovations.

&nbsp; | &nbsp;
------ | ------
**Challenges** | Research data services from the Earth system sciences community are diverse and until now mainly directed to a smaller community, e.g., an institute. We work on a structure in the USN that allows to map the different resources and to access them. The USN will also evaluate if an open community support system (like Stackoverflow) will be of value next to the institutional RDM support of the USN team. To work on that evaluation, we need a solid idea of what kind of user questions are ask-ing, which we expect to get by running the ticketing system.
**Innovations** | **Single point of access to a national expert pool** offering individual support for ESS RDM problems for all phases of the data lifecycle <br><br> Collection, harmonization and provision of **expert knowledge based on institutional experience**, e.g., via Living Handbook  <br><br> Creation of **standard operation procedures (SOPs)** for user support
**Users**	| We envision the following types of primary users:<br>Users, who are looking for **general information**, e.g., on NFDI4Earth tools or on **how to use NFDI4Earth services** <br> Users, who are looking for **support in ESS research data management (RDM)**
**Interface(s)** | User interface
**Unit of adoption** |	Individuals <br>Research institutions

The User Support Network can be contacted via: https://onestop4all.nfdi4earth.de/?support

## Whitebox NFDI4Earth-funded Community Software

The NFDI4Earth-funded community software includes NFDI4Earth Pilots and Incubators.

### Blackbox NFDI4Earth Pilots

The blackbox description is based on the Pilots concept one-pager [https://doi.org/10.5281/zenodo.7583596](https://doi.org/10.5281/zenodo.7583596).

The NFDI4Earth Earth System Science (ESS) Pilots are small projects from various disciplines of the ESS community usually lasting for one year. Pilots are used to assess and define requirements in other task areas and promising results will be integrated into the NFDI4Earth infrastructure.

&nbsp; | &nbsp;
------ | ------
**Problem**	| To achieve acceptance and adoption of the community as well as a cultural change, NFDI4Earth must not implement top-down solutions but involve ideas and existing tools from the research community. Different domains of ESS face different challenges in interoperability, standardization of data, methods and workflows. Expertise and technologies are existent but need further development to meet domain specific requirements and often lack transferability for usage beyond a small user group.
**Innovations**	|	**Agile projects that directly reflect researchers' needs** in data management and implement novel solutions for research data management<br><br>**Bottom-up innovation scouts** for other Task Areas of NFDI4Earth<br><br>Focus on **transferability** of results and **enhancement of technologies** to make use of existing resources and foster community driven design of NFDI4Earth
**Users**	| The target community are researchers from the ESS community working on tools that enhance research data management. The solutions implemented from the pilots are targeted to the respective scientific community.
**Interface(s)** | Depending on the individual pilot proposals
**Unit of adoption** | NFDI4Earth takes up pilots' results into their infrastructure <br> **User communities** of different domains that adopt the newly de-veloped tools by pilots

As the pilots provide individual solutions, we don’t include the whitebox description here.<br>
NFDI4Earth Pilots are available at: https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots

### Blackbox NFDI4Earth Incubators
NFDI4Earth Incubators are documented here: https://nfdi4earth.de/2participate/incubator-lab