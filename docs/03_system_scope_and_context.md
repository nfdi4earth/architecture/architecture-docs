# System Scope and Context

<!---
**Contents**

System scope and context - as the name suggests - delimits your system (i.e. your scope) from all its communication partners (neighboring systems and users, i.e. the context of your system).
It thereby specifies the external interfaces.

If necessary, differentiate the business context (domain specific inputs and outputs) from the technical context (channels, protocols, hardware).

**Motivation**

The domain interfaces and technical interfaces to communication partners are among your system's most critical aspects.
Make sure that you completely understand them.

**Form**

Various options:
-   Context diagrams
-   Lists of communication partners and their interfaces.

See [Context and Scope](https://docs.arc42.org/section-3/) in the arc42 documentation.
-->

Today, there are numerous scattered and heterogeneous services supporting RDM in ESS. Some of them are project-based and do not have a long-term perspective or are not openly available to all researchers. In addition, the implementation of RDM concepts, FAIR principles and related concepts differ among researchers, institutions, and disciplines. NFDI4Earth contributes to aligning existing and emerging RDM services along FAIR and Openness principles and to working towards a unifying and long-term perspective for services. Thus, NFDI4Earth targets the consolidation and harmonization of research data-related services in ESS and linking these services into the NFDI4Earth software architecture (see https://doi.org/10.5281/zenodo.5718943).  <br>
<br>
The development of the NFDI4Earth architecture is supported and influenced by several external drivers. For instance, the architecture reuses / integrates well-known and accepted existing services, components, and resources from the ESS community and infrastructure providers. Moreover, the NFDI4Earth architecture benefits and contributes to the activities of the national research data infrastructure (NFDI) in case of 1) consuming / integrating NFDI basic services (see https://base4nfdi.de/projects) and 2) developing strategies for reusing / integrating services across NFDI consortia.
The following aspects are beyond the scope of the NFDI4Earth Software Architecture:

- Guarantee for availability and functional correctness of integrated services: NFDI4Earth cannot ensure availability and functional correctness of integrated services. However, existing information on both aspects will be made available for the users. <br>
- Correctness of harvested or linked (meta) data: Harvested or linked (meta) data are provided as-is by referencing the data source and information on curation / quality assurance when available <br>
- Completeness of harvested or linked (meta) data: NFDI4Earth integrates existing services with no claim to completeness. <br>
- Up-to-dateness of information: NFDI4Earth will systematically evaluate several information during the project period. Due to the amount of harvested and provided information, NFDI4Earth will apply different strategies for the different resource types and data sources, e.g., a community approach to update or to notify needs for information update for exclusively provided (within NFDI4Earth manually collected) information. <br>
- Compliance for specifications and standards: NFDI4Earth will not check compliance for external sources, but will provide existing information from the providers. <br>
- Hosting and maintenance of all existing (external) project-based ESS services after funding: NFDI4Earth will not host and maintain all existing (external) ESS services after the end of the services’ project periods, but will first evaluate a proposed service against relevant functional and non-functional requirements. <br>


<!-- ## Business Context -->

<!---
**Contents**

Specification of **all** communication partners (users, IT-systems, ...) with explanations of domain specific inputs and outputs or interfaces.
Optionally you can add domain specific formats or communication protocols.

**Motivation**

All stakeholders should understand which data are exchanged with the environment of the system.

**Form**

All kinds of diagrams that show the system as a black box and specify the domain interfaces to communication partners.

Alternatively (or additionally) you can use a table.
The title of the table is the name of your system, the three columns contain the name of the communication partner, the inputs, and the outputs.

**\<Diagram or Table>**

**\<optionally: Explanation of external domain interfaces>**
-->


<!-- ## Technical Context -->

<!---
**Contents**

Technical interfaces (channels and transmission media) linking your system to its environment. In addition a mapping of domain specific input/output to the channels, i.e. an explanation which I/O uses which channel.

**Motivation**

Many stakeholders make architectural decision based on the technical interfaces between the system and its context.
Especially infrastructure or hardware designers decide these technical interfaces.

**Form**

E.g. UML deployment diagram describing channels to neighboring systems, together with a mapping table showing the relationships between channels and input/output.

**\<Diagram or Table>**
**\<optionally: Explanation of technical interfaces>**
**\<Mapping Input/Output to Channels>**
-->

