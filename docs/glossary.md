# Glossary

<!--
The most important domain and technical terms that your stakeholders use
when discussing the system.
You can also see the glossary as source for translations if you work in
multi-language teams.

You should clearly define your terms, so that all stakeholders

-   have an identical understanding of these terms

-   do not use synonyms and homonyms

A table with columns \<Term> and \<Definition>.

Potentially more columns in case you need translations.

See [Glossary](https://docs.arc42.org/section-12/) in the arc42
documentation.

+-----------------------+-----------------------------------------------+
| Term                  | Definition                                    |
+=======================+===============================================+
| *CRUD*           | *\<definition-1>*                             |
+-----------------------+-----------------------------------------------+
| *\<Term-2>*           | *\<definition-2>*                             |
+-----------------------+-----------------------------------------------+

-->

<!---
Formatting as a Definition List: https://michelf.ca/projects/php-markdown/extra/#def-list
-->

API
:   Application Programming Interface

CES
:   Customer Effort Score

CRUD
:   Basic operations on a digital artefact are create, read, update, and delete, often abbreviated to "[CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)".

CSAT
:   Customer Satisfaction Score

Digital Object Identifier
DOI
:   _In computing, a Digital Object Identifier or DOI is a persistent identifier or handle used to uniquely identify objects [..]_
    _A DOI aims to be "resolvable", usually to some form of access to the information object to which the DOI refers._

    [via [Wikipedia](https://en.wikipedia.org/wiki/Digital_object_identifier), see also [https://doi.org](https://doi.org)]

Knowledge Hub
:   ...

Literate programming
:   _Literate programming is a programming paradigm [..] in which a program is given as an explanation of the program logic in a natural language, such as English, interspersed with snippets of macros and traditional source code, from which a compilable source code can be generated._

    [via [Wikipedia](https://en.wikipedia.org/wiki/Literate_programming)]

Living Handbook
:   ...

NPS
:   Net Promoter Score

Research products
: Research products is used here as a catch-all term that includes

  1. datasets,
  2. services,
  3. tools,
  4. vocabularies,
  5. reports,
  6. scientific papers,
  7. peer reviews, and
     
     many other results of the scientific process.