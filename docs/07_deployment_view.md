{% import 'includes.md' as includes %}

# Deployment View

## Infrastructure Level 1

In this section, we present the high-level architecture and key components that form the foundation of our deployment strategy. This section provides an overarching view of the core structures, their interactions, and their collective role in facilitating the deployment, and operational aspects of our system.

### System Distribution

Our system is distributed across various virtual machines (VMs), all located in the [Lehmann-Zentrum Data Center](https://tu-dresden.de/zih/die-einrichtung/rechenzentrums-infrastruktur/lehmann-zentrum-rechenzentrum-lzr) within the so called [Enterprise Cloud](https://tu-dresden.de/zih/dienste/service-katalog/zusammenarbeiten-und-forschen/server_hosting/virtuelle_server) service structure.

#### Network structure

{% if not extra.enable_pdf_export %}
![Overview](diagrams/NetworkStructure.drawio)
{% else %}
![Overview of network structure](diagrams/NetworkStructure-Overview.png)
{% endif %}
<small style="opacity: .5">
    <i>Diagram of NFDI4Earth's network structure
        <a href="{{ config.repo_url }}/diagrams/NetworkStructure.drawio">(source)</a>
    </i>
</small>

### Motivations for the Deployment Structure

 The Center for Information Services and High-Performance Computing (ZIH) was established to provide a robust and reliable infrastructure for the various services and applications of the TUD Dresden University of Technology. By leveraging the established infrastructure, we benefit from the expertise and resources of the ZIH, which facilitates the maintenance and scalability of our application.

Our system is distributed across multiple virtual machines (VMs), following the ZIH's guideline of having one service per VM. This approach has several advantages:

- **Load Balancing**: Distributing services across multiple VMs allows for better load balancing. If one service experiences high traffic, it will not affect the performance of the other services.

- **Fault Isolation**: If one VM fails, only the service on that VM is affected. The other services continue to run on their respective VMs, minimizing the impact of the failure.

- **Test Environment**: Having separate VMs for each service facilitates the management of a test environment. We can replicate the production environments on a dedicated set of VMs for testing purposes without affecting the live services.

### Efficient Management and Deployment of Applications with Docker and Portainer

#### General Docker setup

All applications are deployed as Docker containers due to the following advantages:

**1. Isolation and Consistency:** Containers provide a consistent environment across the development, testing, and production phases.

**2. Portability:** Applications can run seamlessly on different platforms and cloud services.

**3. Resource Efficiency:** Containers run with minimal overhead and utilize system resources more efficiently, even when deployed on virtual machines.

**4. Rapid Deployment and Rollbacks:** New versions can be deployed quickly and rolled back if needed.

**5. Simplified Dependency Management:** All dependencies are bundled within the container.

**6. Port Forwarding:** Docker simplifies port forwarding, allowing easy mapping of host ports to container ports, and ensuring external access to services.

**7. Persistent Storage with Volumes:** Docker volumes enable persistent data storage independent of the container lifecycle, ensuring data durability and easy sharing between containers.

#### Portainer

We utilize [Portainer](https://docs.portainer.io) (in Community Edition) to manage our Docker containers because it meets our needs effectively as it's free, open-source, and benefits from active community support and ongoing development, ensuring regular updates and new features. Portainer's capabilities are particularly advantageous in centralizing our container management interface.

Portainer serves as a unified UI deployed centrally within our designated [virtual machine](#vm-support-apps-productive). From here, it seamlessly integrates and centrally manages all Docker hosts (VMs) across different environments. This setup allows us to efficiently oversee and administer our containerized applications from a single interface. Each Docker host is treated as an environment within Portainer, ensuring streamlined operations and enhancing our ability to monitor and maintain our infrastructure effectively.

{% if not extra.enable_pdf_export %}
![PortainerSetup](diagrams/Ansible.drawio)
{% else %}
![Overview of portainer setup](diagrams/Ansible-PortainerSetup.png)
{% endif %}
<small style="opacity: .5">
    <i>General Portainer Setup
        <a href="{{ config.repo_url }}/diagrams/Ansible.drawio">(source)</a>
    </i>
</small>

Portainer and Portainer agents are implemented as Docker containers themselves. Portainer serves as a centralized management interface deployed as a container while Portainer agents are deployed on each Docker host (VM) to facilitate communication and management tasks between Portainer and the Docker environment.

{{ includes.highlighted_link('Open Portainer UI', extra.portainer.url) }}

### Automating Deployment using Ansible

We implemented our entire deployment process using Ansible, a powerful automation tool that offers several advantages.

Ansible features an agentless architecture, leveraging standard SSH connections for seamless setup and reduced overhead. It ensures idempotency, meaning running the same playbook multiple times achieves a consistent state without unintended changes. Using YAML for playbooks makes Ansible easy to read and write, lowering the barrier to entry and accelerating automation script development.

{{ includes.highlighted_linklist(
    'Automatic Deployment - Links',
    [
        {'name': 'Documentation', 'url': extra.ansible.documentation},
        {'name': 'GitLab Project', 'url': extra.ansible.gitlab_url},
        {'name': 'Ansible - general documentation', 'url': 'https://docs.ansible.com'},
    ]
) }}

##### Potential Drawbacks of Using Ansible

**Performance Overhead:** Ansible uses SSH for communication, and thus, might be slower than agent-based tools when managing many machines simultaneously. This can lead to longer deployment times for extensive infrastructures.

**Learning Curve:** While Ansible is relatively easy to learn, mastering its more advanced features and best practices can take time and effort, particularly for users new to configuration management and automation.

**Complexity in Large Environments:** Managing big and complex infrastructures with Ansible can become challenging, requiring careful organization of playbooks, roles, and inventories to maintain readability and manageability.

#### Maintenance & Documentation

To manage our Ansible scripts comprehensively, we have centralized them within a GitLab project for documentation and version control.

The Ansible deployment scripts, playbooks, and documentation are only accessible to authenticated users.

{{ includes.highlighted_link('Open Ansible scripts & documentation', extra.ansible.gitlab_url) }}

#### Handling secrets

Given the distributed nature of our developers and facilities, we have implemented a secure handling of host_vars in Ansible. This approach ensures that access to sensitive information, such as secrets and credentials, is restricted. Not everyone has access to all host_vars, ensuring that each team member only sees and manages the variables relevant to their role and permissions. This strategy enhances security and confidentiality while facilitating efficient collaboration across our diverse team and infrastructure setup.

{% if not extra.enable_pdf_export %}
![HandlingHostVars](diagrams/Ansible.drawio)
{% else %}
![Overview how host variables are handled](diagrams/Ansible-HandlingHostVars.png)
{% endif %}
<small style="opacity: .5">
    <i>Diagram about handling of Ansible host_vars
        <a href="{{ config.repo_url }}/diagrams/Ansible.drawio">(source)</a>
    </i>
</small>


### Version Control

{{ includes.highlighted_linklist(
    'Links',
    [
        {'name': 'GitLab - Main group', 'url': extra.gitlab.main_group},
    ]
) }}

In collaborative software development, especially within large teams, using a version control system (VCS) is essential. A VCS allows multiple developers to work on the same codebase simultaneously, track changes, and maintain a complete history of modifications. This enables seamless collaboration, as team or group members can work independently on different features or bug fixes without interfering with each other's work. It also facilitates code reviews, debugging, and project management by providing a clear record of who made which changes and why.

#### Using Git

Git is particularly suited for this purpose due to its distributed nature. Unlike centralized version control systems, Git allows every developer to have a complete local copy of the repository, including its full history. This means that developers can work offline and commit changes locally, which can later be synchronized with the central repository. Git's branching and merging capabilities are also highly advanced, making it easier to manage different development streams and integrate changes from various team members. Its speed, efficiency, and wide adoption make Git a robust choice for version control in collaborative environments.

#### Centralized Management with GitLab

In addition to using a distributed version control system like Git, having a centralized instance for managing repositories is crucial for coordinated development efforts. Managing a central instance ensures that there is a single source of truth for the project's codebase, facilitating easier integration, continuous integration/continuous deployment (CI/CD), and unified access control. This centralization helps in maintaining consistency, security, and reliability across the development process.

GitLab is a good choice for this central instance due to its comprehensive suite of tools designed for modern software development. Therefore, we use the GitLab instance provided by RWTH Aachen, which offers several advantages for our needs. As a research institution, we need to utilize platforms that are hosted within the academic and publicly funded sector rather than commercial solutions. The RWTH Aachen instance supports seamless integration with GitHub for authentication, providing a convenient login method while aligning with our requirements for an open, non-commercial environment.

#### GitLab

##### Groups

We have set up various groups within the RWTH Aachen GitLab instance, each aligned with specific software components of our project.

{{ includes.load_gitlab_maingroups() }}

##### Members

{{ includes.load_gitlab_user_stats() }}

### Domains

#### Structure and subdomains

Our infrastructure relies on three distinct domain types to manage and access our services effectively:

**n4e.geo.tu-dresden.de**: This internal domain serves as the operational backbone for our services within the TU Dresden network:

{{ includes.highlighted_linklist(
    'Subdomains for *n4e.geo.tu-dresden.de*',
    get_domains(extra.domains, 'intern')
) }}

**nfdi4earth.de**: Acting as a public-facing domain alias, nfdi4earth.de promotes our services to external users by redirecting them to our internal domain.

{{ includes.highlighted_linklist(
    'Subdomains for *nfdi4arth.de*',
    [{'name': 'Main Website', 'url': 'https://' + extra.domains.base.public}] +
    get_domains(extra.domains, 'public')
) }}

**test.n4e.geo.tu-dresden.de**: Reserved for testing purposes, this domain is accessible only within the TU Dresden network, ensuring secure development and evaluation environments.

{{ includes.highlighted_linklist(
    'Subdomains for *test.n4e.geo.tu-dresden.de*',
    get_domains(extra.domains, 'test')
) }}

#### SSL Certificates

SSL certificates are uniformly generated using Sectigo, ACME, and Certbot. Since early 2023, TU Dresden has relied on Sectigo as its SSL certificate provider. Sectigo serves as a Certificate Authority (CA) issuing trusted certificates. ACME (Automated Certificate Management Environment) acts as the protocol for automated certificate issuance and management, while Certbot, an open-source tool, uses ACME to obtain and install SSL certificates from Let's Encrypt. This collaboration ensures efficient and secure SSL certificate management for TU Dresden's various services and domains.

Furthermore, the SSL certificates are regularly checked for updates. This ongoing process is managed through systemctl timers, which automate the monitoring and updating tasks. Specifically, the renewal of SSL certificates is scheduled to occur 30 days before their expiration date. The status of those timers can be checked at the virtual machines by running: `systemctl list-timers | grep certbot`


## Infrastructure Level 2

This section provides detailed information on virtual machines (VMs) and Docker containers and their configurations.

We provide dedicated virtual machines (VMs) for the test and production instances of our services. This separation facilitates testing new features and updates in an isolated environment before being deployed to the production instance. The advantages of this setup include increased stability and reliability of the production environment, reduced risk of downtime or service disruptions, and the ability to identify and resolve issues in the test instance without affecting end-users.

In addition, we also maintain two VMs that serve a supporting and aggregating role. These VMs host various services, enhancing the overall functionality.

### OneStop4All Deployment Overview

The OneStop4All service is a crucial component of the NFDI4Earth project, designed to support researchers in finding and accessing earth system-related resources and services (see also [blackbox](#onestop4all) & [whitebox](#white-box-onestop4all) descriptions). This platform utilizes modern technologies to provide a user-friendly interface for searching and managing geo-related research resources.

{{ includes.highlighted_linklist(
    'OneStop4All - Links',
    [
        {'name': 'OneStop4All - Blackbox description', 'url': '#onestop4all'},
        {'name': 'OneStop4All - Whitebox description', 'url': '#white-box-onestop4all'},
        {'name': 'Main GitLab group', 'url': extra.onestop4all.gitlab_group_url},
        {'name': '52North - main website', 'url': extra.onestop4all.fiftytwonorth.main_website},
        {'name': '52North - Open Pioneer Trails', 'url': extra.onestop4all.fiftytwonorth.pioneer},
    ]
) }}

#### Technical Implementation

The technical implementation is based on the Open Pioneer Trails framework, which utilizes React, Chakra UI, Vite, and pnpm. This framework offers a modern development environment with efficient build tools and a flexible runtime.

It is composed of three Dockerized components:

##### Frontend

Delivers the user interface for accessing and managing resources, the OneStop4All service is built upon the Open Pioneer Trails framework developed by 52°North. This framework leverages modern technologies including React, Chakra UI, Vite, and pnpm, ensuring a robust and user-friendly platform for geo-related research data management.

##### Index

Indexes data from KnowledgeHub into [Apache Solr](https://solr.apache.org), facilitating efficient search operations.

##### Harvester

This component systematically retrieves information from the KnowledgeHub's TripleStore using SPARQL queries. It prepares this extracted data for integration into the Index component. This process significantly enhances the platform's ability to deliver fast and efficient data retrieval for users accessing the frontend interface.

##### Deployment

Like all major services, the OneStop4All is deployed on two dedicated virtual machines (VMs) to distinguish between testing and production environments (see also [System Distribution](#system-distribution)). Following the [main deployment strategy](#general-docker-setup) this deployment strategy leverages Docker for containerization, allowing for consistent and isolated environments for each component of the application. Docker Compose is used for orchestration, which simplifies the management of multi-container applications by defining and running them with ease. This setup provides streamlined operations, efficient resource management, and scalability to accommodate growing needs.

#### VM - OneStop4All (Productive)

##### System infos

{{ includes.load_sys_info('sysinfo_onestop4all_prod.json') }}

##### Docker container

{{ includes.load_docker_info('dockerinfo_onestop4all_prod.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_onestop4all_prod.json') }}

#### VM - OneStop4All (Test)

##### System infos

{{ includes.load_sys_info('sysinfo_onestop4all_test.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_onestop4all_test.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_onestop4all_test.json') }}

### KnowledgeHub Deployment Overview

The Knowledge Hub is a central backend for data storage, leveraging modern technologies like Linked Open Data.
See [blackbox](#knowledge-hub) & [whitebox](#white-box-knowledge-hub) for further descriptions

{{ includes.highlighted_linklist(
    'KnowledgeHub - Links',
    [
        {'name': 'KnowledgeHub - Blackbox description', 'url': '#knowledge-hub'},
        {'name': 'KnowledgeHub - Whitebox description', 'url': '#white-box-knowledge-hub'},
        {'name': 'Main GitLab group', 'url': extra.knowledgehub.gitlab_group_url},
    ]
) }}

#### Technical Implementation

The Knowledge Hub is composed of three main components:

##### Harvesting Scripts and Pipelines

Developed in Python, these scripts are responsible for collecting and processing data from various sources.

##### Middleware

Cordra is used for managing the ingestion and updates of the triple store, providing a robust interface for data operations.

##### Triple Store

Jena Fuseki is utilized as the triple store, offering a powerful and scalable solution for storing and querying RDF data.

{{ includes.highlighted_linklist(
    'KnowledgeHub - source code',
    [
        {'name': 'KnowledgeHub - full backend setup', 'url': extra.knowledgehub.gitlab_backend },
        {'name': 'KnowledgeHub - harvesting scripts', 'url': extra.knowledgehub.gitlab_populator },
    ]
) }}

#### Deployment

Like all major services, the Knowledge Hub is deployed on two dedicated virtual machines (VMs) to distinguish between testing and production environments (see also [System Distribution](#system-distribution)). Following the [main deployment strategy](#general-docker-setup) this deployment strategy leverages Docker for containerization, allowing for consistent and isolated environments for each component of the application. Docker Compose is used for orchestration, which simplifies the management of multi-container applications by defining and running them with ease. This setup provides streamlined operations, efficient resource management, and scalability to accommodate growing needs.

##### Update cycles

The harvesting scripts are executed regularly according to the schedule depicted in the enclosed graphic. This automation is achieved through the use of [Celery](https://docs.celeryq.dev/en/stable/#) and [RabbitMQ](https://www.rabbitmq.com). Celery is a distributed task queue that enables the asynchronous execution of tasks, ensuring that the harvesting scripts run at the designated intervals. And RabbitMQ acts as the message broker, facilitating communication between the Celery workers and the task queue. This setup ensures that the harvesting processes are efficiently managed and executed in a timely manner.

![Schedule for update cycles](images/07_knowledgehub_schedule.png)

#### VM - KnowledgeHub (Productive)

##### System infos

{{ includes.load_sys_info('sysinfo_knowledgehub_prod.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_knowledgehub_prod.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_knowledgehub_prod.json') }}

#### VM - KnowledgeHub (Test)

##### System infos

{{ includes.load_sys_info('sysinfo_knowledgehub_test.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_knowledgehub_test.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_knowledgehub_test.json') }}

### EduTrain Deployment Overview

The central portal for the EduTrain service is a Learning Management System (LMS) implemented using [Open edX]({{ extra.edutrain.openedx.main_url }}). Open edX is a robust, open-source platform designed to create, deliver, and manage online courses.

Both instances (test and production) of  Open edX are maintained using [Tutor]({{ extra.edutrain.tutor.main_url }}), a command-line tool that simplifies the deployment and management of Open edX. Tutor uses Docker containers to encapsulate all necessary components, ensuring a consistent and reproducible environment. This setup includes containers for the LMS, content management, database, and other services, enabling easy scaling, updates, and maintenance of the EduTrain service.

{{ includes.highlighted_linklist(
    'EduTrain - Links',
    [
        {'name': 'EduTrain - Blackbox description', 'url': '#edutrain'},
        {'name': 'Main GitLab group', 'url': extra.edutrain.gitlab_group_url},
        {'name': 'Open edX - main docu', 'url': extra.edutrain.openedx.main_url},
        {'name': 'Tutor - main docu', 'url': extra.edutrain.tutor.main_url},
    ]
) }}

#### General maintenance

{% if not extra.enable_pdf_export %}
![OpenedXDeployment](diagrams/OpenedX.drawio)
{% else %}
![Overview how Open edX gets deployed](diagrams/OpenedX-OpenedXDeployment.png)
{% endif %}
<small style="opacity: .5">
    <i>Open edX - general maintenance
        <a href="{{ config.repo_url }}/diagrams/OpenedX.drawio">(source)</a>
    </i>
</small>

#### VM - EduTrain (Productive)

##### System infos

{{ includes.load_sys_info('sysinfo_edutrain_prod.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_edutrain_prod.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_edutrain_prod.json') }}

#### VM - EduTrain (Test)

##### System infos

{{ includes.load_sys_info('sysinfo_edutrain_test.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_edutrain_test.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_edutrain_test.json') }}

#### Specific maintenance

##### Subdomains

Open edX requires various subdomains to differentiate and direct users to different areas within the platform. These subdomains are essential for organizing and navigating distinct functionalities such as courses, administrative tools, user profiles, and interactive content.

![(sub) domains](diagrams/OpenedX.drawio)
<small style="opacity: .5">
    <i>Open edX - Subdomains
        <a href="{{ config.repo_url }}/diagrams/OpenedX.drawio">(source)</a>
    </i>
</small>

##### Apply adapted theme

We have developed a custom theme to provide a unique and cohesive user experience along all NFDI4Earth products. This theme is managed and published as a [GitLab project]({{ extra.edutrain.tutor.theme.gitlab_url }}), ensuring organized version control and easy collaboration among our development team, on the one hand. And simple deployment on test- and production system, on the other hand.

{{ includes.highlighted_linklist(
    'Open edX - NFDI4Earth theme',
    [
        {'name': 'GitLab - project', 'url': extra.edutrain.tutor.theme.gitlab_url},
        {'name': 'Tutor - theming docu', 'url': extra.edutrain.tutor.theme.docu_url},
    ]
) }}

##### Integration of interactive Jupyter notebooks in courses

A specialized setup has been implemented to seamlessly integrate interactive Jupyter notebooks directly into the courses. This allows for dynamic, hands-on content to be embedded within the Open edX platform. By incorporating Jupyter notebooks, the interactivity and practical learning experience are enhanced, enabling students to execute code, visualize data, and explore concepts in real-time without leaving the course environment.

![OpenedXandJupyterHub](diagrams/OpenedX.drawio)
<small style="opacity: .5">
    <i>Open edX - general maintenance
        <a href="{{ config.repo_url }}/diagrams/OpenedX.drawio">(source)</a>
    </i>
</small>

A separate JupyterHub instance is required for this setup which is running on the [Support-Apps VM](#vm-support-apps-productive). Authentication is managed through Learning Technology Interoperability (LTI) and Jupyter XBlocks. Additionally, notebooks are pulled directly from GitLab using nbgitpuller, ensuring that the latest content is always available to learners.

{{ includes.highlighted_linklist(
    'Open edX & Jupyter notebooks',
    [
        {'name': 'GitLab - notebooks', 'url': extra.edutrain.materials.notebooks},
        {'name': 'Open edX - LTI docu', 'url': extra.edutrain.openedx.lti_docu},
        {'name': 'Open edX - Jupyterhub - xblock', 'url': extra.edutrain.openedx.jupyter_xblock_docu},
        {'name': 'Jupyterhub - LTI docu', 'url': extra.edutrain.jupyterhub.lti_docu},
        {'name': 'Jupyterhub - LTI authenticator', 'url': extra.edutrain.jupyterhub.lti_authenticator},
        {'name': 'Jupyterhub - nbgitpuller', 'url': extra.edutrain.jupyterhub.nbgitpuller},
    ]
) }}

### Support Apps Deployment Overview

This instance exists solely as a production instance and deploys all services using Docker containers. Its purpose is to provide various support services that aid in the overall infrastructure management and operation. For example, it hosts the [Portainer UI](#portainer). This approach ensures that essential support tools are readily available and efficiently managed.

{{ includes.highlighted_linklist(
    'SupportApps - Links',
    [
        {'name': 'Portainer UI', 'url': extra.portainer },
        {'name': 'Landing pages - production version', 'url': extra.supportapps.landingpages.productive },
        {'name': 'Landing pages - staging version', 'url': extra.supportapps.landingpages.staging },
        {'name': 'Landing pages - GitLab project', 'url': extra.supportapps.landingpages.gitlab_url },
        {'name': 'Jupyterhub (*not accessible)', 'url': extra.supportapps.jupyterhub },
    ]
) }}

#### Supportive applications/services

##### JupyterHub

JupyterHub is a multi-user server for Jupyter notebooks, enabling users to work with notebook documents via a web interface. It provides a convenient way to serve notebooks to a predefined group of users, supports multiple instances, and integrates with various authentication mechanisms, making it highly suitable for educational, corporate, and scientific computing environments. JupyterHub simplifies the process of managing dependencies and environments for users, allowing them to focus on their computational tasks rather than setup and configuration.

The JupyterHub, exclusively utilized in our production environment, is configured with a specialized authentication method (see [here]({{ extra.edutrain.openedx.lti_docu }})). This configuration implies that the service is not directly accessible but can only be used through the EduTrain portal. For further details on implementation and usage, please refer to the [dedicated section here](#integration-of-interactive-jupyter-notebooks-in-courses).

##### Portainer

See [dedicated section](#portainer)!

##### LandingPages

This service provides various landing pages for different virtual machines (VMs) within NFDI4Earth.
Landing pages act as targeted entry points for specific content allowing  tracking of user interactions, and ensure efficient access to resources.

**Landing Page for Knowledge Hub**: A dedicated page offering access and resources related to the KnowledgeHub, which serves as a central repository for the organization's knowledge management system.

**Landing Page for WebApps VM**: A focused page that provides links, resources, and documentation for various web applications hosted on this VM.

**Landing Page for the Landing Pages Management**: A meta-landing page that gives an overview and management capabilities for the other landing pages, ensuring they are up-to-date and accessible. This is essential for testing them easily in a staging phase.

This is a centrally set up [project]({{ extra.supportapps.landingpages.gitlab_url }}) hosted via the [support-apps VM](#supportapps-deployment-overview), deployed using a GitLab [CI/CD pipeline]({{ extra.supportapps.landingpages.gitlab_url }}#the-pipeline).

All of them are generally accessed via the basic domain of the service (see [domains section](#domains)) pointing to the dedicated virtual machine (see [network structure](#network-structure)). The virtual machine has a nginx webserver running, which has a proxy setup pointing to the main domain of the support apps VM

```mermaid
graph LR
    LP("GitLab project at `/var/www`")
    LPurl("landing-pages.support-apps.n4e.geo.tu-dresden.de")
    subgraph SA["SupportApps VM"]
        SA_nginx["nginx"]
        SA_nginx -->|config| LP
        LP -->|is subdirectory| kh(/knowledgehub)
        LP -->|is subdirectory| wa(/webapps)
    end
    subgraph KH["KnowledgeHub VM"]
        KH_nginx["nginx"] -->|config| KH_proxy("proxy")
    end
    subgraph WA["Webapps VM"]
        WA_nginx["nginx"] -->|config| WA_proxy("proxy")
    end
    KH_proxy --> LPurl
    WA_proxy --> LPurl
    LPurl --> SA_nginx
    KH_Domain["knowledgehub.nfdi4earth.de"] --> KH_nginx
    WA_Domain["webapps.nfdi4earth.de"] --> WA_nginx
```

##### GitLab Runner

The provided GitLab Runner service is a tool that executes CI/CD (Continuous Integration/Continuous Deployment) pipelines in GitLab. It runs jobs configured in GitLab CI/CD across various platforms and environments.  It serves as the central instance where we register all the runners needed for our GitLab projects.

#### VM - Support-Apps (Productive)

##### System infos

{{ includes.load_sys_info('sysinfo_supportapps_prod.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_supportapps_prod.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_supportapps_prod.json') }}


### Webapps Deployment Overview

On the "webapps" VM, we host a variety of applications developed by the community. These are primarily pilot and incubator projects, but can also include other initiatives. Following our general deployment strategy, these applications are deployed as Docker containers. They are indexed on a [simple landing page]({{ extra.webapps.main_url }}) and published using short subdomains.

Since this is not a central service, no distinction is made between test and production instances, allowing for rapid deployment and iteration of these community-driven projects.

{{ includes.highlighted_linklist(
    'Webapps - Links',
    [
        {'name': 'Major landing page', 'url': extra.webapps.main_url },
    ]
) }}

#### VM - Web-Apps (Productive)

##### System infos

{{ includes.load_sys_info('sysinfo_webapps_prod.json') }}

##### Docker setup

{{ includes.load_docker_info('dockerinfo_webapps_prod.json') }}

##### SSL Certificate info

{{ includes.load_ssl_info('sslinfo_webapps_prod.json') }}

### General contact

- <a href="mailto:helpdesk@nfdi4earth.de">helpdesk@nfdi4earth.de</a>
- Ralf Klammer (ralf.klammer@tu-dresden.de)

