# Acknowledgements

This specification and guides are developed by the members of the project NFDI4Earth - <https://nfdi4earth.de/>.
This work has been funded by the German Research Foundation (DFG) through the project NFDI4Earth (TA4 M4.3, DFG project no. 460036893) within the German National Research Data Infrastructure (NFDI, <https://www.nfdi.de/>).

[![NFDI4Earth Logo](images/n4e-logo.png)](https://nfdi4earth.de/)

To cite this specification please use the first published version of the NFDI4Earth architecture documentation: 
Degbelo, A., Frickenhaus, S., Grieb, J., Hachinger, S., Henzen, C., Klammer, R., Müller, C., Munke, J., Nüst, D., Purr, C., Weiland, C., & Wellmann, A. (2023). NFDI4Earth Software Architecture. Zenodo. https://doi.org/10.5281/zenodo.8333959 (Authors in alphabetical order)

For a complete list of publications, posters, presentations, and software projects by NFD4Earth, please visit <https://nfdi4earth.de/outputs>.

## About arc42

arc42, the Template for documentation of software and system architecture.

Template Version 8.2 EN. (based upon AsciiDoc version), January 2023

Created, maintained and © by Dr. Peter Hruschka, Dr. Gernot Starke and contributors. See <https://arc42.org>.

# License

![CC-0 Button](https://licensebuttons.net/p/zero/1.0/88x31.png)

The NFDI4Earth architecture specification is licensed under [Creative Commons CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/), see file [`LICENSE`](https://git.rwth-aachen.de/nfdi4earth/softwaretoolsarchitecture/architecture-docs/-/blob/master/LICENSE).
To the extent possible under law, the people who associated CC0 with this work have waived all copyright and related or neighboring rights to this work.
This work is published from: Germany.

------

<div class="buildinfo">Build @@VERSION@@ @ @@TIMESTAMP@@</div>
