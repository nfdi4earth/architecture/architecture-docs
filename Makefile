.EXPORT_ALL_VARIABLES:
TOKEN_GITLAB_GROUP_STATS ?= $(shell bash -c 'read -p "Unable to connect to GitLab, please set: TOKEN_GITLAB_GROUP_STATS=" token; echo $$token')

default: serve

clean:
	rm -rf public/

clean_test:
	rm -rf test/; 

serve:
	mkdocs serve

serve_dev:
	mkdocs serve --dev-addr localhost:8081

test: clean_test
	ENABLE_PDF_EXPORT=1 mkdocs build --verbose --site-dir test

build:
	ENABLE_PDF_EXPORT=1 mkdocs build --verbose
	# update version in cover page, if not already done
	sed -i 's/@@VERSION@@/${VCS_REF}/g' public/index.html
	sed -i 's/@@TIMESTAMP@@/${CURRENT_DATE}/g' public/index.html

build_ci:
	ENABLE_PDF_EXPORT=0 mkdocs build --verbose
	# update version in cover page, if not already done
	sed -i 's/@@VERSION@@/${VCS_REF}/g' public/index.html
	sed -i 's/@@TIMESTAMP@@/${CURRENT_DATE}/g' public/index.html

VCS_REF := $(shell git rev-parse --short HEAD)
CURRENT_DATE := $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
FILE_NAME_PDF := $(shell echo nfdi4earth-architecture-${VCS_REF}.pdf)

pdf: build
	# fix protocol relative URLs, see https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2713
	find public/ -type f -name '*.html' | xargs sed -i 's|href="//|href="https://|g'
	find public/ -type f -name '*.html' | xargs sed -i 's|src="//|src="https://|g'
	# create PDF
	wkhtmltopdf --margin-top 20mm --enable-local-file-access --load-error-handling ignore $(shell pwd)/public/index.html public/nfdi4earth-architecture.pdf
