# NFDI4Earth Software Architecture

Project website: <https://git.rwth-aachen.de/nfdi4earth/architecture/architecture-docs>

**Read architecture description online: <https://nfdi4earth.pages.rwth-aachen.de/architecture/architecture-docs/>**

## The project

The NFDI4Earth Software Architecture Documentation provides a documentation of the NFDI4Earth software architecture. The documentation summarizes the major user cases, gives an overview on the requirement analysis approaches for the NFDI4Earth services, points out envisioned quality goals and potential architecture’s stakeholders. Getting more specific, the documentation provides externally and internally driven constraints that need to be considered when developing or contributing to NFDI4Earth services. We herewith facilitate, the users to better understand the solution strategy for the NFDI4Earth architecture.<br>
We use the arc42 template for the documentation of software and system architecture. The detailed description of the NFDI4Earth architecture is done by using a blackbox-whitebox approach that describes the major aim, the main functionalities and interfaces of the services as seen by the users first (blackbox) and then provides details on the (inner) solutions, e.g., service components, implementations (whitebox). <br>
The envisioned target group of the documentation includes software developers and architects as well service providers. 
<br><br>
The current version of the NFDI4Earth Software Architecture Documentation is published on Zenodo: <br>
Henzen, C., Brauer, A., Degbelo, A., Frickenhaus, S., Grieb, J., Hachinger, S., Klammer, R., Müller, C., Munke, J., Niers, T., Nüst, D., Weiland, C., & Wellmann, A. (2024). NFDI4Earth Software Architecture Documentation. Zenodo. https://doi.org/10.5281/zenodo.14534839

## Guidelines

### Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md)

### Build docs

This specification is written in [Markdown](https://daringfireball.net/projects/markdown/), rendered with [MkDocs](http://www.mkdocs.org/) using a few [Python Markdown extensions](https://pythonhosted.org/Markdown/extensions/index.html), and deployed automatically using CI.

Use `mkdocs` to render it locally.

```bash
# initialise Python environment, e.g.: mkvirtualenv n4r-architecture
# install tools: pip install -r requirements.txt
mkdocs serve
```

Now open <http://127.0.0.1:8000/>.

### Build PDF

```bash
# apt-get install wkhtmltopdf (version >= 0.12.6)
make pdf
```

### Automated Builds

See [`.gitlab-ci.yml`](.gitlab-ci.yml).

#### Scheduled pipeline

See [pipeline-schedules](https://git.rwth-aachen.de/nfdi4earth/architecture/architecture-docs/-/pipeline_schedules)

## Diagrams

The diagrams are created with [yEd](https://www.yworks.com/products/yed).
All source files (`.graphml`) are stored in `/docs/img`.
The PNG renderings are exported from yEd with `transparent` background, a margin of `5`, and a scaling factor of `1.0`.

## Plugins

- [MkDocs Plugin for embedding Diagrams.net (Draw.io)](https://github.com/onixpro/mkdocs-drawio-file)
- [MkDocs-Macros](https://github.com/fralau/mkdocs-macros-plugin)

## Issues

### PDF

We have 2 issues regarding integrated pdf export (via `with-pdf`):

1. the 'cinder'-theme produces an error:

```bash
ERROR   -  Could not load theme handler cinder: No module named 'mkdocs_with_pdf.themes.cinder'
```

2. the latest version of `pydyf==0.10` produces an integrated deprecation warning, which is noticed in strict-mode.

Both issues disable `mkdocs build` in strict-mode!

### GitLab Group Stats

Did you run into the following exception?

```bash
    raise Exception("GitLab access token undefined - see README!")
```

That means the Macro-module is not able to request the GitLab statistics on groups and users correctly. Because the corresponding environment variable `TOKEN_GITLAB_GROUP_STATS` is not know in you terminal session!

Solution:

Get the main one by checking the CI/CD variables for `TOKEN_GITLAB_GROUP_STATS` or set an individual, which enables you to request the main group "NFDI4Earth" via api and set it accordingly:

Option 1.: `export TOKEN_GITLAB_GROUP_STATS={ token }`

Option 2: run MAKEFILE and you will get a prompt, if it is not already set.


## ToDos

- clarify if deployment shall be in '--strict' mode or not, cause of issues with pydyf & cinder

## License

The NFDI4Earth Software Architecture is licensed under [Creative Commons CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/), see file `LICENSE`.
To the extent possible under law, the people who associated CC0 with this work have waived all copyright and related or neighboring rights to this work.
This work is published from: Germany.
