# Contributing to this project

## Content

All contributions are welcome.

Please take a look at the overview on arc42 at <https://arc42.org/overview>.

Especially individual groups and people responsible for specific parts of the NFDI4Earth architecture are invited to propose changes in form of merge requests.
The arc42 structure gives great flexibility to go into different levels of detail for only parst of the architecture, so it is expected that different needs and preferences of components within the NFDI4Earth architecture can be met.

A member of the NFDI4Earth architecture team reviews the proposal and the team uses the merge request discussion to give feedback.
In case the proponent is a member of the architecture team themself, a different member of the team must accept the changes.

## Writing style & formatting

- Put each sentence on one line.
- Use **bold text to highlight words in a paragaph that readers should see when the skim the text.
  Use _italic_ text to higlight words within a sentence while reading a paragraph.
- Comments

  ```bash
  <!-- This is a standard html comment which will remain in the output. -->
  <!--- This is a markdown comment which this extension removes. -->
  ```
  
  Based on <https://github.com/ryneeverett/python-markdown-comments>.
- Use HTML-tags for headers that shall not show up in the table of contents:
  
  ```md
  <h3>Szenarios</h3> <!-- not in TOC -->
  ```
- Use the HTML tag `<mark>` for (inline) highlighting of text, e.g.,
  
  ```md
  <mark>TODO</mark>
  ```

## Merge requests

In your first pull request, please state you are contributing under the project license.
