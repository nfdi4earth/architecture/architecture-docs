import os

from datetime import datetime
from json import loads
from requests.sessions import Session


GRAPHQL_QUERY_MEMBERS = """
query {
    group(fullPath: "nfdi4earth") {
        fullName
        webUrl
        groupMembers {
        edges {
            node {
                user {
                    name
                    username
                    bot
                    webUrl
                }
            }
        }
        }
        descendantGroups {
        edges {
            node {
            fullName
            name
            webUrl
            groupMembers {
                nodes {
                    user {
                        name
                        username
                        bot
                        webUrl
                    }
                }
            }
            }
        }
        }
    }
}
"""

GRAPHQL_QUERY_SUBGROUPS = """
query {
  group(fullPath: "nfdi4earth") {
    name
    descendantGroups(includeParentDescendants: false) {
      nodes {
        name
        fullPath
        webUrl
      }
    }
  }
}
"""


def get_group_stats_token():
    if "TOKEN_GITLAB_GROUP_STATS" in os.environ:
        return os.environ["TOKEN_GITLAB_GROUP_STATS"]
    else:
        raise Exception("GitLab access token undefined - see README!")


def define_env(env):
    """
    Define macros for the mkdocs environment
    """
    def request(access_token, url_sub=None, headers={}, data={}, method="get"):
        headers["PRIVATE-TOKEN"] = access_token
        _method = getattr(Session(), method)
        gitlab_graphql_url = env.variables["extra"]["gitlab"]["graphql_url"]
        return _method(
            gitlab_graphql_url + url_sub, headers=headers, data=data)

    @env.macro
    def load_jsonfile(filename, category):
        # Load the content of a json the file
        # and return it as a list of dictionaries
        # -> possible categories: docker, system
        f = open(f"docs/external_exports/{filename}", "r")
        content = loads(f.read())
        f.close()
        if category == 'docker':
            # Format the Ports field for the docker category
            for c in content['containers']:
                c['Ports'] = [
                    f"{p['PublicPort']}:{p['PrivatePort']}" 
                    for p in c['Ports'] 
                    if 'PublicPort' in p
                ]
        elif category == 'system':
            for m in content['sysinfo']['mounts']:
                if m["device"] == "/dev/mapper/main-root":
                    disk = {}
                    disk['total'] = round(
                        m['size_total'] / (1024 * 1024 * 1024), 2)
                    disk['available'] = round(
                        m['size_available'] / (1024 * 1024 * 1024), 2)
                    disk['percentage'] = round(
                        m['size_available'] / m['size_total'] * 100, 2)
                    content['sysinfo']['disk'] = disk
        elif category == 'ssl':
            infos = ''
            for line in content['infos'].split('\n'):
                if line.find('Certificate Name') != -1:
                    infos += '\n\n'
                    infos += line.replace('  ', '')
                    infos += '\n'
                else:
                    infos += '\n- ' + line
            content['infos'] = infos
        return content

    @env.macro
    def load_markdownfile(filename, category):
        f = open(f"docs/external_exports/{filename}", "r")
        if category == 'ssl':
            content = ''
            for l in f.readlines():
                if l.find('Certificate Name') != -1:
                    content += '\n\n'
                    content += l.replace('  ', '')
                    content += '\n'
                else:
                    content += '- ' + l
            # r = f.read().replace('    ', '\n  - ')
            return content
        else:
            return f.read()

    @env.macro
    def get_domains(domains, category):
        urls = []
        for sub in domains['subs']:
            if category == 'test' and not sub['has_test_instance']:
                continue
            urls.append({
                'name': sub['name'],
                'url': f"https://{sub['domain']}.{domains['base'][category]}"
            })
        return urls

    @env.macro
    def get_gitlab_maingroups():
        response = request(
            get_group_stats_token(),
            url_sub="",
            method="post",
            data={"query": GRAPHQL_QUERY_SUBGROUPS},
        ).json()

        return {
            "groups": response["data"]["group"]["descendantGroups"]["nodes"],
            "date": datetime.strftime(datetime.now(), "%Y-%m-%d"),
        }

    @env.macro
    def get_gitlab_user_stats():
        response = request(
            get_group_stats_token(),
            url_sub="",
            method="post",
            data={"query": GRAPHQL_QUERY_MEMBERS},
        ).json()
        group_data = response["data"]["group"]
        users = [""]
        user_urls = {}
        # build a list of users
        for edge in group_data["descendantGroups"]["edges"]:
            data = edge["node"]
            for member in data["groupMembers"]["nodes"]:
                user = member["user"]
                # exclude:
                #   - empty users
                #   - users, that are already in the list
                #   - BOTS (e.g. tokens with API-Acess)
                if user is not None and user["name"] not in users and user["bot"] is False:
                    users.append(user["name"])
                    user_urls[user["name"]] = user['webUrl']
        # sort users
        users = sorted(users)
        # init 'rows' list
        groups = []

        # build 1st row for main group
        row = ["-" for c in range(len(users))]
        row[0] = {"name": group_data["fullName"], "url": group_data["webUrl"]}
        for member in group_data["groupMembers"]["edges"]:
            user = member["node"]["user"]
            if user is not None and user["name"] in users:
                # replace 'empty' list entry by 'X' at the corresponding position
                row[users.index(user["name"])] = "X"
        groups.append(row)

        # build row for each sub-group of 'NFDI4Earth'
        for edge in group_data["descendantGroups"]["edges"]:
            data = edge["node"]
            # initialize 'empty' row
            row = ["-" for c in range(len(users))]
            # add group name as first element
            row[0] = {"name": data["fullName"], "url": data["webUrl"]}
            for member in data["groupMembers"]["nodes"]:
                user = member["user"]
                if user is not None and user["name"] in users:
                    # replace 'empty' list entry by 'X' at the corresponding position
                    row[users.index(user["name"])] = "X"
            groups.append(row)
        # sort groups by group name
        groups = sorted(groups, key=lambda item: (item[0]['name']))

        return {
            "users": [
                {"name": user_name, "url": user_urls[user_name]}
                for user_name in users[1:]
            ],
            "groups": groups,
            "date": datetime.strftime(datetime.now(), "%Y-%m-%d"),
        }
